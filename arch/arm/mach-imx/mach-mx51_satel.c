/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 * Copyright (C) 2009-2010 Amit Kucheria <amit.kucheria@canonical.com>
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/input.h>
#include <linux/spi/flash.h>
#include <linux/spi/spi.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>

#include "common.h"
#include "devices-imx51.h"
#include "hardware.h"
#include "iomux-mx51.h"


#define BABBAGE_USBH1_STP	IMX_GPIO_NR(1, 27)
#define SATEL_FEC_PHY_RESET	IMX_GPIO_NR(2, 2)
#define SATEL_FEC_BOOT_LEG	IMX_GPIO_NR(2, 0)
#define BABBAGE_ECSPI1_CS0	IMX_GPIO_NR(4, 24)
#define BABBAGE_ECSPI1_CS1	IMX_GPIO_NR(4, 25)

#define SATEL_LED1	IMX_GPIO_NR(2, 6) /* Heartbeat */
#define SATEL_LED2	IMX_GPIO_NR(2, 8)
#define SATEL_LED3	IMX_GPIO_NR(2, 4)

/* USB_CTRL_1 */
#define MX51_USB_CTRL_1_OFFSET			0x10
#define MX51_USB_CTRL_UH1_EXT_CLK_EN		(1 << 25)

#define	MX51_USB_PLLDIV_12_MHZ		0x00
#define	MX51_USB_PLL_DIV_19_2_MHZ	0x01
#define	MX51_USB_PLL_DIV_24_MHZ	0x02

static iomux_v3_cfg_t mx51satel_pads[] = {
	/* UART1 */
	MX51_PAD_UART1_RXD__UART1_RXD,
	MX51_PAD_UART1_TXD__UART1_TXD,

	/* UART2 */
	MX51_PAD_UART2_RXD__UART2_RXD,
	MX51_PAD_UART2_TXD__UART2_TXD,

	/* I2C1 */
	MX51_PAD_SD2_CLK__I2C1_SDA,
	MX51_PAD_SD2_CMD__I2C1_SCL,

	/* I2C2 */
	MX51_PAD_KEY_COL4__I2C2_SCL,
	MX51_PAD_KEY_COL5__I2C2_SDA,

	/* FEC */
	MX51_PAD_EIM_EB2__FEC_MDIO,
	MX51_PAD_EIM_EB3__FEC_RDATA1,
	MX51_PAD_EIM_CS2__FEC_RDATA2,
	MX51_PAD_EIM_CS3__FEC_RDATA3,
	MX51_PAD_EIM_CS4__FEC_RX_ER,
	MX51_PAD_EIM_CS5__FEC_CRS,
	MX51_PAD_NANDF_RB2__FEC_COL,
	MX51_PAD_NANDF_RB3__FEC_RX_CLK,
	MX51_PAD_NANDF_D9__FEC_RDATA0,
	MX51_PAD_NANDF_D8__FEC_TDATA0,
	MX51_PAD_NANDF_CS2__FEC_TX_ER,
	MX51_PAD_NANDF_CS3__FEC_MDC,
	MX51_PAD_NANDF_CS4__FEC_TDATA1,
	MX51_PAD_NANDF_CS5__FEC_TDATA2,
	MX51_PAD_NANDF_CS6__FEC_TDATA3,
	MX51_PAD_NANDF_CS7__FEC_TX_EN,
	MX51_PAD_NANDF_RDY_INT__FEC_TX_CLK,

	/* FEC PHY reset line */
	MX51_PAD_EIM_D18__GPIO2_2,
	MX51_PAD_EIM_D16__GPIO2_0,

	/* SD 1 */
	MX51_PAD_SD1_CMD__SD1_CMD,
	MX51_PAD_SD1_CLK__SD1_CLK,
	MX51_PAD_SD1_DATA0__SD1_DATA0,
	MX51_PAD_SD1_DATA1__SD1_DATA1,
	MX51_PAD_SD1_DATA2__SD1_DATA2,
	MX51_PAD_SD1_DATA3__SD1_DATA3,
	/* CD/WP from controller */
	MX51_PAD_GPIO1_0__SD1_CD,
	MX51_PAD_GPIO1_1__SD1_WP,

	/* eCSPI1 */
	MX51_PAD_CSPI1_MISO__ECSPI1_MISO,
	MX51_PAD_CSPI1_MOSI__ECSPI1_MOSI,
	MX51_PAD_CSPI1_SCLK__ECSPI1_SCLK,
	MX51_PAD_CSPI1_SS0__GPIO4_24,
	MX51_PAD_CSPI1_SS1__GPIO4_25,

	/* Audio */
	MX51_PAD_AUD3_BB_TXD__AUD3_TXD,
	MX51_PAD_AUD3_BB_RXD__AUD3_RXD,
	MX51_PAD_AUD3_BB_CK__AUD3_TXC,
	MX51_PAD_AUD3_BB_FS__AUD3_TXFS,

	/* LEDs */
	MX51_PAD_EIM_D22__GPIO2_6,
	MX51_PAD_EIM_D24__GPIO2_8,
	MX51_PAD_EIM_D20__GPIO2_4,
};

static const struct imxi2c_platform_data satel_i2c_data __initconst = {
	.bitrate = 100000,
};

static inline void satel_fec_reset(void)
{
	int ret;

	//printk(KERN_ERR"!!!!!!!!!!!!!! FEC Reset gets called now\n");
	/* reset FEC PHY */
	ret = gpio_request_one(SATEL_FEC_PHY_RESET,
					GPIOF_OUT_INIT_LOW, "fec-phy-reset");
	if (ret) {
		printk(KERN_ERR"failed to get GPIO_FEC_PHY_RESET: %d\n", ret);
		return;
	}

	ret = gpio_request_one(SATEL_FEC_BOOT_LEG,
		GPIOF_OUT_INIT_LOW, "fec-boot-leg");
	if (ret) {
		printk(KERN_ERR"failed to get GPIO_FEC_BOOT_LEG: %d\n", ret);
		return;
	}

	gpio_set_value(SATEL_FEC_BOOT_LEG, 1);
	msleep(20);
	gpio_set_value(SATEL_FEC_BOOT_LEG, 0);
	msleep(20);
	gpio_set_value(SATEL_FEC_PHY_RESET, 0);
	msleep(20);
	gpio_set_value(SATEL_FEC_PHY_RESET, 1);
}

/* This function is board specific as the bit mask for the plldiv will also
be different for other Freescale SoCs, thus a common bitmask is not
possible and cannot get place in /plat-mxc/ehci.c.*/
static int initialize_otg_port(struct platform_device *pdev)
{
	u32 v;
	void __iomem *usb_base;
	void __iomem *usbother_base;

	usb_base = ioremap(MX51_USB_OTG_BASE_ADDR, SZ_4K);
	if (!usb_base)
		return -ENOMEM;
	usbother_base = usb_base + MX5_USBOTHER_REGS_OFFSET;

	/* Set the PHY clock to 19.2MHz */
	v = __raw_readl(usbother_base + MXC_USB_PHY_CTR_FUNC2_OFFSET);
	v &= ~MX5_USB_UTMI_PHYCTRL1_PLLDIV_MASK;
	v |= MX51_USB_PLL_DIV_19_2_MHZ;
	__raw_writel(v, usbother_base + MXC_USB_PHY_CTR_FUNC2_OFFSET);
	iounmap(usb_base);

	mdelay(10);

	return mx51_initialize_usb_hw(0, MXC_EHCI_INTERNAL_PHY);
}

static const struct mxc_usbh_platform_data dr_utmi_config __initconst = {
	.init		= initialize_otg_port,
	.portsc	= MXC_EHCI_UTMI_16BIT,
};

static const struct fsl_usb2_platform_data usb_pdata __initconst = {
	.operating_mode	= FSL_USB2_DR_DEVICE,
	.phy_mode	= FSL_USB2_PHY_UTMI_WIDE,
};

static bool otg_mode_host __initdata;

static int __init babbage_otg_mode(char *options)
{
	if (!strcmp(options, "host"))
		otg_mode_host = true;
	else if (!strcmp(options, "device"))
		otg_mode_host = false;
	else
		pr_info("otg_mode neither \"host\" nor \"device\". "
			"Defaulting to device\n");
	return 1;
}
__setup("otg_mode=", babbage_otg_mode);


static int mx51_babbage_spi_cs[] = {
	BABBAGE_ECSPI1_CS0,
	BABBAGE_ECSPI1_CS1,
};

static const struct spi_imx_master mx51_babbage_spi_pdata __initconst = {
	.chipselect     = mx51_babbage_spi_cs,
	.num_chipselect = ARRAY_SIZE(mx51_babbage_spi_cs),
};

static const struct esdhc_platform_data mx51_satel_sd1_data __initconst = {
	.cd_type = ESDHC_CD_CONTROLLER,
	.wp_type = ESDHC_WP_CONTROLLER,
};


void __init imx51_satel_common_init(void)
{
	mxc_iomux_v3_setup_multiple_pads(mx51satel_pads,
					 ARRAY_SIZE(mx51satel_pads));
}

/*
 * Board specific initialization.
 */
static void __init mx51_satel_init(void)
{
	int ret;
	// iomux_v3_cfg_t usbh1stp = MX51_PAD_USBH1_STP__USBH1_STP;
	// iomux_v3_cfg_t power_key = NEW_PAD_CTRL(MX51_PAD_EIM_A27__GPIO2_21,
	// 	PAD_CTL_SRE_FAST | PAD_CTL_DSE_HIGH);

	printk("!!!!!!!!!!!!! mx51_satel_init gets called\n");

	imx51_soc_init();

	imx51_satel_common_init();

	imx51_add_imx_uart(0, NULL);
	imx51_add_imx_uart(1, NULL);

	satel_fec_reset();
	imx51_add_fec(NULL);

	/* Set the PAD settings for the pwr key. */

	imx51_add_imx_i2c(0, &satel_i2c_data);
	imx51_add_imx_i2c(1, &satel_i2c_data);

	if (otg_mode_host)
		imx51_add_mxc_ehci_otg(&dr_utmi_config);
	else {
		initialize_otg_port(NULL);
		imx51_add_fsl_usb2_udc(&usb_pdata);
	}

	imx51_add_sdhci_esdhc_imx(0, &mx51_satel_sd1_data);

	imx51_add_imx2_wdt(0);

	// LED
	ret = gpio_request_one(SATEL_LED2,
					GPIOF_OUT_INIT_LOW, "led-2");
	if (ret) {
		printk(KERN_ERR"failed to get led-2: %d\n", ret);
		return;
	}

		ret = gpio_request_one(SATEL_LED3,
					GPIOF_OUT_INIT_LOW, "led-3");
	if (ret) {
		printk(KERN_ERR"failed to get led-3: %d\n", ret);
		return;
	}


	gpio_set_value(SATEL_LED2, 1);
	gpio_set_value(SATEL_LED3, 0);
}

static void __init mx51_satel_timer_init(void)
{
	mx51_clocks_init(32768, 24000000, 22579200, 0);
}

MACHINE_START(MX51_SATEL, "Freescale i.MX51 SATEL INT_TSI Board")
	/* Maintainer: Pawel Zabielowicz <pawel.zabielowicz@satel.pl> */
	.atag_offset = 0x100,
	.map_io = mx51_map_io,
	.init_early = imx51_init_early,
	.init_irq = mx51_init_irq,
	.handle_irq = imx51_handle_irq,
	.init_time	= mx51_satel_timer_init,
	.init_machine = mx51_satel_init,
	.init_late	= imx51_init_late,
	.restart	= mxc_restart,
MACHINE_END